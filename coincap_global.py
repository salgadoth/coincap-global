from requests import Request, Session 
from requests.exceptions  import ConnectionError, Timeout, TooManyRedirects
import json 

url = 'URL'
parameters = {
	'start' : '1',
	'limit' : '3',
	'convert' : 'USD',
}

headers = {
	'Accepts' : 'aplication/json',
	'X-CMC_PRO_API_KEY' : 'API-KEY',
}

try:
	response = session.get(url, params = parameters)
	data = json.loads(response.text)
	formatted_data = json.dumps(data, sort_keys=True, indent = 2)
	print(formatted_data)
except (ConnectionError, Timeout, TooManyRedirects) as e:
	print(e)

name = formatted_data['data']['name']
symbol = formatted_data['data']['symbol']
rank = formatted_data['data']['cmc_rank']
price = formatted_data['data']['quote']['USD']['price']
last_updatted = formatted_data['data']['quote']['USD']['last_updatted'] 

print("Name..: "+ str(name) +
	  "\nSymbol.....: "+ str(symbol) +
	  "\nRank.......: "+ str(rank) +
	  "\nPrice......: "+ str(price) +
	  "\nLast Update: "+ str(last_updatted))
